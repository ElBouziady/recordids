#-------------------------------------------------
#
# Project created by QtCreator 2018-01-16T10:54:28
#
#-------------------------------------------------

QT       += core gui opengl

CONFIG += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = RecordIDS
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
    src/cameralist.cpp \
    src/eventthread.cpp \
    src/histogram.cpp \
    src/imageinfodlg.cpp \
    src/mainview.cpp \
    src/paintlabel.cpp \
    src/properties.cpp \
    src/qsliderex.cpp \
    src/tabadvanced.cpp \
    src/tabcamera.cpp \
    src/tabformat.cpp \
    src/tabhotpixel.cpp \
    src/tabimage.cpp \
    src/tabio.cpp \
    src/tabprocessing.cpp \
    src/tabsize.cpp \
    src/tabsizexs.cpp \
    src/tabxs.cpp \
    buffers.cpp \
    recorder.cpp

HEADERS  += mainwindow.h \
    src/cameralist.h \
    src/eventthread.h \
    src/histogram.h \
    src/imageinfodlg.h \
    src/mainview.h \
    src/paintlabel.h \
    src/properties.h \
    src/qsliderex.h \
    src/qtui.h \
    src/version.h \
    buffers.h \
    recorder.h

FORMS    += mainwindow.ui \
    src/histogram.ui \
    src/imageinfodlg.ui \
    src/mainview.ui \
    src/properties.ui

RESOURCES += \
    mainview.qrc


DEFINES += PWD=\\\"$$dirname(PWD)\\\"
DEFINES += __user__

##### IDS #####
!win32 {
    # Linux
    DEFINES += __LINUX__
    LIBS += -lueye_api
} else {
    # Windows
    !contains(QMAKE_TARGET.arch, x86_64) {
        LIBS += -lueye_api
    } else {
        LIBS += -lueye_api_64
    }
}

INCLUDEPATH += \
    . \
    include \
    src


##### OpenCV #####
unix {
LIBS += `pkg-config --libs opencv`
LIBS+= -L"/usr/local/lib/"  -lopencv_highgui

INCLUDEPATH += '/usr/local/include'

}

#ifndef RECORDER_H
#define RECORDER_H

#include <QObject>
#include <QThread>
#include <QFile>
#include <QTextStream>
#include <QDate>
#include "buffers.h"
#include "opencv2/opencv.hpp"

class Recorder : public QThread
{
    Q_OBJECT
public:
    Recorder(Buffers* buffer);

    ~Recorder();

    void OpenTimeFile();

    void CloseTimeFile();

    void SetVideoFile(cv::VideoWriter* video_file);

    void SetVideosOrImages(bool is_video);

    void SetRole(std::string camera_role);

    void SetRecordDir(QString dir_path);

private:

    void run();


private:

    Buffers* buffer_ = NULL;
    cv::Mat img_input_;
    std::string role_= "";
    QString record_dir_ = "";
    QFile time_file_;
    QTextStream time_file_text_;

    cv::VideoWriter* video_file_ = NULL;
    bool is_video_ = true;

};

#endif // RECORDER_H

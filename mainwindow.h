﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <QMessageBox>
#include <QCloseEvent>
#include <QTextStream>

#include <ueye.h>

#include <iostream>

#include "src/mainview.h"
#include "buffers.h"
#include "recorder.h"

#include <opencv2/videoio.hpp>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:

    QStringList UpdateCameradIDs();

    void TimerEvent();

    void closeEvent (QCloseEvent *event);

    bool IsConfigValid();

    void SetBlockItems(bool enable);

    void SaveConfig();

    void UpdateGUI();

    bool ConfigCamMono(HIDS hCam);

    bool ConfigCamStereo(HIDS master_hCam,HIDS slave_hCam);

    void on_radioButton_mono_clicked();

    void on_radioButton_stereo_clicked();

    void on_pushButton_path_clicked();

    void on_pushButton_refresh_clicked();

    void on_pushButton_record_clicked();

    void on_pushButton_open_clicked();

    void on_radioButton_pwm_clicked();

    void on_radioButton_flash_clicked();

    void on_pushButton_stop_clicked();

private:
    Ui::MainWindow *ui;

    Mainview* master = NULL;
    Mainview* slave = NULL;

    bool is_recording_;
    QTimer timer_record_;
    QTimer timer_update_;

    cv::VideoWriter video_file_master_;
    cv::VideoWriter video_file_slave_;

    Buffers* buffer_master_ = NULL;
    Buffers* buffer_slave_ = NULL;

    Recorder* recorder_master_ = NULL;
    Recorder* recorder_slave_ = NULL;

};

#endif // MAINWINDOW_H

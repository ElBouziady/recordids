﻿#include "mainwindow.h"
#include "ui_mainwindow.h"

using namespace std;
using namespace cv;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(ui->spinBox_frequency,SIGNAL(valueChanged(int)),ui->horizontalSlider_frequency,SLOT(setValue(int)));
    connect(ui->horizontalSlider_frequency,SIGNAL(valueChanged(int)),ui->spinBox_frequency,SLOT(setValue(int)));
    connect(ui->spinBox_cycle,SIGNAL(valueChanged(int)),ui->horizontalSlider_cycle,SLOT(setValue(int)));
    connect(ui->horizontalSlider_cycle,SIGNAL(valueChanged(int)),ui->spinBox_cycle,SLOT(setValue(int)));
    connect(ui->spinBox_delay,SIGNAL(valueChanged(int)),ui->horizontalSlider_delay,SLOT(setValue(int)));
    connect(ui->horizontalSlider_delay,SIGNAL(valueChanged(int)),ui->spinBox_delay,SLOT(setValue(int)));
    connect(ui->spinBox_duration,SIGNAL(valueChanged(int)),ui->horizontalSlider_duration,SLOT(setValue(int)));
    connect(ui->horizontalSlider_duration,SIGNAL(valueChanged(int)),ui->spinBox_duration,SLOT(setValue(int)));

    connect(&timer_record_, SIGNAL(timeout()), this, SLOT(TimerEvent()));
    connect(&timer_update_, SIGNAL(timeout()), this, SLOT(UpdateGUI()));

    ui->pushButton_refresh->click();
    ui->radioButton_stereo->click();

    is_recording_ = false;

    ui->statusBar->showMessage("No Record");
}

MainWindow::~MainWindow()
{
    delete ui;
}

QStringList MainWindow::UpdateCameradIDs(){

    QStringList cam_ids;
    INT nNumCam;
    if( is_GetNumberOfCameras( &nNumCam ) == IS_SUCCESS) {
        if( nNumCam >= 1 ) {

            // Create new list with suitable size
            UEYE_CAMERA_LIST* pucl = (UEYE_CAMERA_LIST*) new BYTE [sizeof (DWORD) + nNumCam * sizeof (UEYE_CAMERA_INFO)];
            pucl->dwCount = nNumCam;

            //Retrieve camera info
            if (is_GetCameraList(pucl) == IS_SUCCESS) {
                for (int iCamera = 0; iCamera < (int)pucl->dwCount; iCamera++) {
                    cam_ids.push_back(QString::number(pucl->uci[iCamera].dwCameraID));
                }

            }
            delete [] pucl;
        }
    }
    return cam_ids;
}

void MainWindow::closeEvent (QCloseEvent *event)
{
    QMessageBox::StandardButton resBtn = QMessageBox::question( this, "RecordIDS",
                                                                tr("Are you sure you want to close this application ? \n"),
                                                                QMessageBox::No | QMessageBox::Yes,
                                                                QMessageBox::Yes);
    if (resBtn != QMessageBox::Yes) {
        event->ignore();
    } else {
        event->accept();
        if (master != NULL)
            master->close();
        if (slave != NULL)
            slave->close();
    }
}

void MainWindow::on_radioButton_mono_clicked()
{
    ui->label_cam->setEnabled(true);
    ui->comboBox_cam->setEnabled(true);
    ui->label_master->setEnabled(false);
    ui->comboBox_master->setEnabled(false);
    ui->label_slave->setEnabled(false);
    ui->comboBox_slave->setEnabled(false);
    ui->label_baseline->setEnabled(false);
    ui->doubleSpinBox_baseline->setEnabled(false);
    ui->label_synch_error->setEnabled(false);
    ui->lcdNumber_synch_error->setEnabled(false);
    if (ui->radioButton_pwm->isChecked()){
        ui->label_frequency->setEnabled(false);
        ui->label_cycle->setEnabled(false);
        ui->horizontalSlider_frequency->setEnabled(false);
        ui->horizontalSlider_cycle->setEnabled(false);
        ui->spinBox_frequency->setEnabled(false);
        ui->spinBox_cycle->setEnabled(false);
    }
    else {
        ui->label_delay->setEnabled(false);
        ui->label_duration_flash->setEnabled(false);
        ui->horizontalSlider_delay->setEnabled(false);
        ui->horizontalSlider_duration->setEnabled(false);
        ui->spinBox_delay->setEnabled(false);
        ui->spinBox_duration->setEnabled(false);
    }
    ui->radioButton_pwm->setEnabled(false);
    ui->radioButton_flash->setEnabled(false);
}

void MainWindow::on_radioButton_stereo_clicked()
{
    ui->label_cam->setEnabled(false);
    ui->comboBox_cam->setEnabled(false);
    ui->label_master->setEnabled(true);
    ui->comboBox_master->setEnabled(true);
    ui->label_slave->setEnabled(true);
    ui->comboBox_slave->setEnabled(true);
    ui->label_baseline->setEnabled(true);
    ui->doubleSpinBox_baseline->setEnabled(true);
    ui->label_synch_error->setEnabled(true);
    ui->lcdNumber_synch_error->setEnabled(true);
    ui->radioButton_pwm->setEnabled(true);
    ui->radioButton_flash->setEnabled(true);
    ui->radioButton_pwm->click();
}

void MainWindow::on_pushButton_path_clicked()
{
    QString record_path = QFileDialog::getExistingDirectory(this, tr("Open Record Directory"),PWD"/Dataset",QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
    if (record_path.size() != 0)
        ui->lineEdit_path->setText(record_path);
    else
        return;

    if (ui->radioButton_mono->isChecked()){
        if (master != NULL){
            if (ui->radioButton_images->isChecked()){
                QDir().mkdir(ui->lineEdit_path->text()+"/1");
                recorder_master_->SetVideosOrImages(false);
            }
            recorder_master_->SetRecordDir(ui->lineEdit_path->text()+"/1");
            buffer_master_->ClearDataProcessing();

            if (ui->radioButton_videos->isChecked()){
                double fps;
                IS_SIZE_2D size;

                is_SetFrameRate (ui->comboBox_cam->currentText().toInt(), IS_GET_FRAMERATE, &fps);
                is_AOI(ui->comboBox_cam->currentText().toInt(), IS_AOI_IMAGE_GET_SIZE, (void*)&size, sizeof(size));

                video_file_master_.open(QString(ui->lineEdit_path->text()+"/1.avi").toStdString(), 0, (int)fps, Size(size.s32Width,size.s32Height), false);
                if (!video_file_master_.isOpened())
                {
                    cout  << "Could not open the video file : " << ui->lineEdit_path->text().toStdString() << "/1.avi" << endl;
                    return;
                }
                recorder_master_->SetVideoFile(&video_file_master_);
                recorder_master_->SetVideosOrImages(true);
            }
        }
    }
    else  {
        if (master != NULL){
            if (ui->radioButton_images->isChecked()){
                QDir().mkdir(ui->lineEdit_path->text()+"/1");
                recorder_master_->SetVideosOrImages(false);
            }
            recorder_master_->SetRecordDir(ui->lineEdit_path->text()+"/1");
            buffer_master_->ClearDataProcessing();

            if (ui->radioButton_videos->isChecked()){
                double fps;
                IS_SIZE_2D size;
                IO_PWM_PARAMS m_pwmParams;

                is_IO(ui->comboBox_master->currentText().toInt(), IS_IO_CMD_PWM_GET_PARAMS,(void*)&m_pwmParams, sizeof(m_pwmParams));
                fps = cvRound(m_pwmParams.dblFrequency_Hz);
                is_AOI(ui->comboBox_master->currentText().toInt(), IS_AOI_IMAGE_GET_SIZE, (void*)&size, sizeof(size));


                video_file_master_.open(QString(ui->lineEdit_path->text()+"/1.avi").toStdString(), 0, (int)fps, Size(size.s32Width,size.s32Height), false);
                if (!video_file_master_.isOpened())
                {
                    cout  << "Could not open the video file : " << ui->lineEdit_path->text().toStdString() << "/1.avi" << endl;
                    return;
                }
                recorder_master_->SetVideoFile(&video_file_master_);
                recorder_master_->SetVideosOrImages(true);
            }
        }
        if (slave != NULL){
            if (ui->radioButton_images->isChecked()){
                QDir().mkdir(ui->lineEdit_path->text()+"/2");
                recorder_slave_->SetVideosOrImages(false);
            }
            recorder_slave_->SetRecordDir(ui->lineEdit_path->text()+"/2");
            buffer_slave_->ClearDataProcessing();

            if (ui->radioButton_videos->isChecked()){

                double fps;
                IS_SIZE_2D size;
                IO_PWM_PARAMS m_pwmParams;

                is_IO(ui->comboBox_master->currentText().toInt(), IS_IO_CMD_PWM_GET_PARAMS,(void*)&m_pwmParams, sizeof(m_pwmParams));
                fps = cvRound(m_pwmParams.dblFrequency_Hz);
                is_AOI(ui->comboBox_slave->currentText().toInt(), IS_AOI_IMAGE_GET_SIZE, (void*)&size, sizeof(size));


                video_file_slave_.open(QString(ui->lineEdit_path->text()+"/2.avi").toStdString(), 0, (int)fps, Size(size.s32Width,size.s32Height), false);
                if (!video_file_slave_.isOpened())
                {
                    cout  << "Could not open the video file : " << ui->lineEdit_path->text().toStdString() << "/2.avi" << endl;
                    return;
                }
                recorder_slave_->SetVideoFile(&video_file_slave_);
                recorder_slave_->SetVideosOrImages(true);
            }
        }
    }
}

void MainWindow::on_pushButton_refresh_clicked()
{
    ui->comboBox_cam->clear();
    ui->comboBox_cam->addItems(UpdateCameradIDs());

    ui->comboBox_master->clear();
    ui->comboBox_master->addItems(UpdateCameradIDs());

    ui->comboBox_slave->clear();
    ui->comboBox_slave->addItems(UpdateCameradIDs());

     if (ui->radioButton_stereo->isChecked())
         ui->comboBox_slave->setCurrentIndex(1);

}

void MainWindow::on_pushButton_record_clicked()
{
    if (ui->progressBar_record->value() == 100){
        ui->progressBar_record->setValue(0);
        master->ResetImageIndex();
        if (ui->radioButton_stereo->isChecked())
            slave->ResetImageIndex();
    }

    ui->pushButton_stop->setEnabled(true);
    ui->label_hour->setEnabled(false);
    ui->label_min->setEnabled(false);
    ui->spinBox_hour->setEnabled(false);
    ui->spinBox_min->setEnabled(false);
    ui->label_duration->setEnabled(false);

    is_recording_ = !is_recording_;

    buffer_master_->recording_active_ = is_recording_;
    if (is_recording_)
        recorder_master_->start();
    else
        recorder_master_->exit();
    if (ui->radioButton_stereo->isChecked()){
        recorder_slave_->OpenTimeFile();
        buffer_slave_->recording_active_ = is_recording_;
        if (is_recording_)
            recorder_slave_->start();
        else
            recorder_slave_->exit();
    }
    else
        recorder_master_->OpenTimeFile();


    long long unsigned int record_duration = ui->spinBox_hour->value()*3600*1000 + ui->spinBox_min->value()*60*1000;
    if (is_recording_){
        ui->pushButton_record->setText("Pause");
        ui->statusBar->showMessage("Recording ...");
        timer_record_.start(record_duration/100);
    }
    else {
        ui->pushButton_record->setText("Record");
        ui->statusBar->showMessage("Pause Recording ...");
        timer_record_.stop();
    }
}


void MainWindow::on_pushButton_stop_clicked()
{
    sleep(2);
    ui->pushButton_stop->setEnabled(false);
    ui->label_hour->setEnabled(true);
    ui->label_min->setEnabled(true);
    ui->spinBox_hour->setEnabled(true);
    ui->spinBox_min->setEnabled(true);
    ui->label_duration->setEnabled(true);
    is_recording_ = false;

    if (ui->radioButton_mono->isChecked()){
        if (ui->radioButton_videos->isChecked()){
            video_file_master_.release();
        }
        buffer_master_->recording_active_ = false;
        recorder_master_->exit();
        master->SaveCamParams(ui->lineEdit_path->text()+"/cam.ini");
        recorder_master_->CloseTimeFile();
    }
    else {
        if (ui->radioButton_videos->isChecked()){
            video_file_master_.release();
            video_file_slave_.release();
        }
        buffer_master_->recording_active_ = false;
        buffer_slave_->recording_active_ = false;
        recorder_master_->exit();
        recorder_slave_->exit();
        master->SaveCamParams(ui->lineEdit_path->text()+"/master.ini");
        slave->SaveCamParams(ui->lineEdit_path->text()+"/slave.ini");
        recorder_slave_->CloseTimeFile();
    }
    SaveConfig();
    timer_record_.stop();
    ui->progressBar_record->setValue(100);
    ui->pushButton_record->setText("Record");
    ui->statusBar->showMessage("Recording Finshed");
}

void MainWindow::TimerEvent()
{
    int value = this->ui->progressBar_record->value();
    if (value+1 < 100)
        ui->progressBar_record->setValue(value+1);
    else
        ui->pushButton_stop->click();
}

void MainWindow::on_pushButton_open_clicked()
{
    if (ui->pushButton_open->text() == "Close Camera(s)"){
        disconnect(master,SIGNAL(sendCameraClosed()),ui->pushButton_open,SLOT(click()));
        master->close();
        delete master;
        master = NULL;
        if (ui->radioButton_stereo->isChecked()){
            disconnect(slave,SIGNAL(sendCameraClosed()),ui->pushButton_open,SLOT(click()));
            slave->close();
            delete slave;
            slave = NULL;
        }
        SetBlockItems(true);
        ui->pushButton_open->setText("Open Camera(s)");
        return;
    }

    if (!IsConfigValid()){
        QMessageBox msgbox;
        msgbox.setText(QString("Invalid Parameters"));
        msgbox.setIcon(QMessageBox::Critical);
        msgbox.exec();
        return;
    }

    timer_update_.start(500);
    SetBlockItems(false);
    ui->pushButton_open->setText("Close Camera(s)");
    ui->lcdNumber_synch_error->display(0);
    if (ui->radioButton_mono->isChecked()){
        ConfigCamMono(ui->comboBox_cam->currentText().toInt());
        master = new Mainview(0,ui->comboBox_cam->currentText());
        connect(master,SIGNAL(sendCameraClosed()),ui->pushButton_open,SLOT(click()));
        master->SetRole("Mono Camera");
        master->setWindowTitle("Mono Camera ID : "+ui->comboBox_master->currentText());
        master->SetCamID(ui->comboBox_cam->currentText().toInt());

        IS_SIZE_2D size;
        is_AOI(ui->comboBox_cam->currentText().toInt(), IS_AOI_IMAGE_GET_SIZE, (void*)&size, sizeof(size));

        buffer_master_ = new Buffers(size.s32Width,size.s32Height);
        recorder_master_ = new Recorder(buffer_master_);
        master->SetBuffer(buffer_master_);
        recorder_master_->SetRole("Mono Camera");

        recorder_master_->SetRecordDir(ui->lineEdit_path->text()+"/1");
        if (ui->radioButton_images->isChecked()){
            QDir().mkdir(ui->lineEdit_path->text()+"/1");
            recorder_master_->SetVideosOrImages(false);
        }

        if (ui->radioButton_videos->isChecked()){
            double fps;
            is_SetFrameRate (ui->comboBox_cam->currentText().toInt(), IS_GET_FRAMERATE, &fps);

            video_file_master_.open(QString(ui->lineEdit_path->text()+"/1.avi").toStdString(), 0, (int)fps, Size(size.s32Width,size.s32Height), false);
            if (!video_file_master_.isOpened())
            {
                cout  << "Could not open the video file : " << ui->lineEdit_path->text().toStdString() << "/1.avi" << endl;
                return;
            }
            recorder_master_->SetVideoFile(&video_file_master_);
            recorder_master_->SetVideosOrImages(true);
        }

        master->show();
    }
    else {
        ConfigCamStereo(ui->comboBox_master->currentText().toInt(),ui->comboBox_slave->currentText().toInt());
        master = new Mainview(0,ui->comboBox_master->currentText());
        slave = new Mainview(0,ui->comboBox_slave->currentText());
        connect(master,SIGNAL(sendCameraClosed()),ui->pushButton_open,SLOT(click()));
        connect(slave,SIGNAL(sendCameraClosed()),ui->pushButton_open,SLOT(click()));
        master->SetRole("Master");
        slave->SetRole("Slave");
        master->setWindowTitle("Master ID : "+ui->comboBox_master->currentText());
        slave->setWindowTitle("Slave ID : "+ui->comboBox_slave->currentText());
        master->SetCamID(ui->comboBox_master->currentText().toInt());
        slave->SetCamID(ui->comboBox_slave->currentText().toInt());

        IS_SIZE_2D size;
        is_AOI(ui->comboBox_master->currentText().toInt(), IS_AOI_IMAGE_GET_SIZE, (void*)&size, sizeof(size));

        buffer_master_ = new Buffers(size.s32Width,size.s32Height);
        recorder_master_ = new Recorder(buffer_master_);
        master->SetBuffer(buffer_master_);
        recorder_master_->SetRole("Master");

        buffer_slave_ = new Buffers(size.s32Width,size.s32Height);
        recorder_slave_ = new Recorder(buffer_slave_);
        slave->SetBuffer(buffer_slave_);
        recorder_slave_->SetRole("Slave");



        recorder_master_->SetRecordDir(ui->lineEdit_path->text()+"/1");
        recorder_slave_->SetRecordDir(ui->lineEdit_path->text()+"/2");
        if (ui->radioButton_images->isChecked()){
            QDir().mkdir(ui->lineEdit_path->text()+"/1");
            QDir().mkdir(ui->lineEdit_path->text()+"/2");
            recorder_master_->SetVideosOrImages(false);
            recorder_slave_->SetVideosOrImages(false);
        }


        if (ui->radioButton_videos->isChecked()){

            double fps;
            IO_PWM_PARAMS m_pwmParams;

            is_IO(ui->comboBox_master->currentText().toInt(), IS_IO_CMD_PWM_GET_PARAMS,(void*)&m_pwmParams, sizeof(m_pwmParams));
            fps = cvRound(m_pwmParams.dblFrequency_Hz);

            video_file_master_.open(QString(ui->lineEdit_path->text()+"/1.avi").toStdString(), 0, (int)fps, Size(size.s32Width,size.s32Height), false);
            if (!video_file_master_.isOpened())
            {
                cout  << "Could not open the video file : " << ui->lineEdit_path->text().toStdString() << "/1.avi" << endl;
                return;
            }
            recorder_master_->SetVideoFile(&video_file_master_);
            recorder_master_->SetVideosOrImages(true);


            is_AOI(ui->comboBox_slave->currentText().toInt(), IS_AOI_IMAGE_GET_SIZE, (void*)&size, sizeof(size));

            video_file_slave_.open(QString(ui->lineEdit_path->text()+"/2.avi").toStdString(), 0, (int)fps, Size(size.s32Width,size.s32Height), false);
            if (!video_file_slave_.isOpened())
            {
                cout  << "Could not open the video file : " << ui->lineEdit_path->text().toStdString() << "/2.avi" << endl;
                return;
            }
            recorder_slave_->SetVideoFile(&video_file_slave_);
            recorder_slave_->SetVideosOrImages(true);

        }

        master->show();
        slave->show();
    }
}

bool MainWindow::IsConfigValid(){
    if (ui->radioButton_mono->isChecked() && ui->comboBox_cam->currentIndex() == -1)
        return false;
    if (ui->radioButton_stereo->isChecked() && (ui->comboBox_master->currentIndex() == -1 || ui->comboBox_slave->currentIndex() == -1 || (ui->comboBox_master->currentIndex() == ui->comboBox_slave->currentIndex()) || ui->doubleSpinBox_baseline->value() == 0.0f))
        return false;
    if (ui->spinBox_hour->value() == 0 && ui->spinBox_min->value() == 0)
        return false;
    if (ui->doubleSpinBox_focal->value() == 0.0f)
        return false;
    if (ui->lineEdit_path->text() == "")
        return false;
    //    if (ui->doubleSpinBox_pitch_angle->value() == -90)
    //        return false;
    //    if (ui->doubleSpinBox_roll_angle->value() == -90)
    //        return false;
    //    if (ui->doubleSpinBox_cam_height->value() == 0)
    //        return false;
    return true;
}

void MainWindow::SaveConfig(){
    QFile conf_file( ui->lineEdit_path->text()+"/config.ini" );
    if ( conf_file.open(QIODevice::WriteOnly) )
    {
        QTextStream stream( &conf_file );
        if (ui->radioButton_mono->isChecked()){
            stream << "Mode=Mono" << endl;
            stream << "CamID=" << ui->comboBox_cam->currentText().toInt()<<endl;
        }
        else
        {
            stream << "Mode=Stereo" << endl;
            stream << "MasterID=" << ui->comboBox_master->currentText().toInt()<<endl;
            stream << "SlaveID=" << ui->comboBox_slave->currentText().toInt()<<endl;
            stream << "Baseline(cm)=" << ui->doubleSpinBox_baseline->value()<<endl;
            if (ui->radioButton_pwm->isChecked()){
                stream << "Frequency/FPS(Hz)=" << ui->spinBox_frequency->value()<<endl;
                stream << "DutyCycle(%)=" << ui->spinBox_cycle->value()<<endl;
            }
            else {
                stream << "Delay(μs)=" << ui->spinBox_delay->value()<<endl;
                stream << "Duration(μs)=" << ui->spinBox_duration->value()<<endl;
            }
        }
        stream << "FocalLength(mm)=" << ui->doubleSpinBox_focal->value()<<endl;
        stream << "RecordDuration(h)=" << ui->spinBox_hour->value()<<endl;
        stream << "RecordDuration(min)=" << ui->spinBox_min->value()<<endl;
        stream << "RecordDirectory=" << ui->lineEdit_path->text()<<endl;
        stream << "CameraAnglePitch(°)=" << ui->doubleSpinBox_pitch_angle->value()<<endl;
        stream << "CameraAngleRoll(°)=" << ui->doubleSpinBox_roll_angle->value()<<endl;
        stream << "CameraHeight(m)=" << ui->doubleSpinBox_cam_height->value()<<endl;
    }
    conf_file.close();
}

void MainWindow::SetBlockItems(bool enable){
    ui->radioButton_mono->setEnabled(enable);
    ui->radioButton_stereo->setEnabled(enable);

    if (ui->radioButton_mono->isChecked())
    {
        ui->comboBox_cam->setEnabled(enable);
        ui->label_cam->setEnabled(enable);
    }
    else {
        ui->comboBox_master->setEnabled(enable);
        ui->comboBox_slave->setEnabled(enable);
        ui->label_master->setEnabled(enable);
        ui->label_slave->setEnabled(enable);
    }
    ui->pushButton_refresh->setEnabled(enable);

    ui->pushButton_record->setEnabled(!enable);
}

void MainWindow::on_radioButton_pwm_clicked()
{
    ui->label_frequency->setEnabled(true);
    ui->label_cycle->setEnabled(true);
    ui->horizontalSlider_frequency->setEnabled(true);
    ui->horizontalSlider_cycle->setEnabled(true);
    ui->spinBox_frequency->setEnabled(true);
    ui->spinBox_cycle->setEnabled(true);

    ui->label_delay->setEnabled(false);
    ui->label_duration_flash->setEnabled(false);
    ui->horizontalSlider_delay->setEnabled(false);
    ui->horizontalSlider_duration->setEnabled(false);
    ui->spinBox_delay->setEnabled(false);
    ui->spinBox_duration->setEnabled(false);
}

void MainWindow::on_radioButton_flash_clicked()
{
    ui->label_frequency->setEnabled(false);
    ui->label_cycle->setEnabled(false);
    ui->horizontalSlider_frequency->setEnabled(false);
    ui->horizontalSlider_cycle->setEnabled(false);
    ui->spinBox_frequency->setEnabled(false);
    ui->spinBox_cycle->setEnabled(false);

    ui->label_delay->setEnabled(true);
    ui->label_duration_flash->setEnabled(true);
    ui->horizontalSlider_delay->setEnabled(true);
    ui->horizontalSlider_duration->setEnabled(true);
    ui->spinBox_delay->setEnabled(true);
    ui->spinBox_duration->setEnabled(true);
}

bool MainWindow::ConfigCamMono(HIDS hCam){

    if(is_InitCamera(&hCam, NULL)!= IS_SUCCESS){
        return false;
    }
    cout<<"Camera "<<hCam<<" OPENED"<<endl;

    if(is_ParameterSet( hCam, IS_PARAMETERSET_CMD_LOAD_EEPROM, NULL, 0 )!= IS_SUCCESS){
        is_ExitCamera(hCam);
        cout<<"Camera "<<hCam<<" CLOSED !!!!!! Error"<<endl;
        return false;
    }
    cout<<"Parameters Init Done"<<endl;

    is_SetExternalTrigger(hCam, IS_SET_TRIGGER_OFF);

    if(is_ParameterSet( hCam, IS_PARAMETERSET_CMD_SAVE_EEPROM, NULL, 0 )!= IS_SUCCESS){
        is_ExitCamera(hCam);
        cout<<"Camera "<<hCam<<" CLOSED !!!!!! Error"<<endl;
        return false;
    }
    cout<<"Parameters Save Done"<<endl;

    is_ExitCamera(hCam);
    cout<<"Camera "<<hCam<<" CLOSED"<<endl;

    sleep(5);

    return true;
}

bool MainWindow::ConfigCamStereo(HIDS master_hCam,HIDS slave_hCam){
    if(is_InitCamera(&master_hCam, NULL)!= IS_SUCCESS){
        return false;
    }
    cout<<"[Master] : Camera "<<master_hCam<<" OPENED"<<endl;

    if(is_InitCamera(&slave_hCam, NULL)!= IS_SUCCESS){
        is_ExitCamera(master_hCam);
        cout<<"[Master] : Camera "<<master_hCam<<" CLOSED !!!!!! Error"<<endl;
        return false;
    }
    cout<<"[Slave] : Camera "<<slave_hCam<<" OPENED"<<endl;

    if(is_ParameterSet( master_hCam, IS_PARAMETERSET_CMD_LOAD_EEPROM, NULL, 0 )!= IS_SUCCESS){
        is_ExitCamera(master_hCam);
        cout<<"[Master] : Camera "<<master_hCam<<" CLOSED !!!!!! Error"<<endl;
        is_ExitCamera(slave_hCam);
        cout<<"[Slave] : Camera "<<slave_hCam<<" CLOSED !!!!!! Error"<<endl;
        return false;
    }
    cout<<"[Master] : Parameters Init Done"<<endl;

    if(is_ParameterSet( slave_hCam, IS_PARAMETERSET_CMD_LOAD_EEPROM, NULL, 0 )!= IS_SUCCESS){
        is_ExitCamera(master_hCam);
        cout<<"[Master] : Camera "<<master_hCam<<" CLOSED !!!!!! Error"<<endl;
        is_ExitCamera(slave_hCam);
        cout<<"[Slave] : Camera "<<slave_hCam<<" CLOSED !!!!!! Error"<<endl;
        return false;
    }
    cout<<"[Slave] : Parameters Init Done"<<endl;

    is_SetExternalTrigger(master_hCam, IS_SET_TRIGGER_HI_LO);
    is_SetExternalTrigger(slave_hCam, IS_SET_TRIGGER_HI_LO);

    IO_GPIO_CONFIGURATION gpioConfiguration;

    // Set configuration of Master GPIO2 Trigger
    gpioConfiguration.u32Gpio = IO_GPIO_2;
    gpioConfiguration.u32Configuration = IS_GPIO_TRIGGER;
    gpioConfiguration.u32State = 0;
    is_IO(master_hCam, IS_IO_CMD_GPIOS_SET_CONFIGURATION, (void*)&gpioConfiguration,sizeof(gpioConfiguration));

    // Set configuration of Slave GPIO2 Trigger
    gpioConfiguration.u32Gpio = IO_GPIO_2;
    gpioConfiguration.u32Configuration = IS_GPIO_TRIGGER;
    gpioConfiguration.u32State = 0;
    is_IO(slave_hCam, IS_IO_CMD_GPIOS_SET_CONFIGURATION, (void*)&gpioConfiguration,sizeof(gpioConfiguration));

    // Set configuration of Master GPIO1
    gpioConfiguration.u32Gpio = IO_GPIO_1;
    gpioConfiguration.u32State = 0;

    if (ui->radioButton_pwm->isChecked()) {
        gpioConfiguration.u32Configuration = IS_GPIO_PWM;

        UINT nMode = IO_GPIO_1;
        is_IO(master_hCam, IS_IO_CMD_PWM_SET_MODE,(void*)&nMode, sizeof(nMode));
        IO_PWM_PARAMS m_pwmParams;
        m_pwmParams.dblFrequency_Hz = ui->spinBox_frequency->value();
        m_pwmParams.dblDutyCycle = ui->spinBox_cycle->value()/100.00;
        is_IO(master_hCam, IS_IO_CMD_PWM_SET_PARAMS,(void*)&m_pwmParams, sizeof(m_pwmParams));
    }
    else {
        gpioConfiguration.u32Configuration = IS_GPIO_FLASH;

        IO_FLASH_PARAMS flashParams;
        flashParams.s32Delay = ui->spinBox_delay->value();
        flashParams.u32Duration = ui->spinBox_duration->value();
        is_IO(master_hCam, IS_IO_CMD_FLASH_SET_PARAMS,(void*)&flashParams, sizeof(flashParams));

    }
    is_IO(master_hCam, IS_IO_CMD_GPIOS_SET_CONFIGURATION, (void*)&gpioConfiguration,sizeof(gpioConfiguration));

    if(is_ParameterSet( master_hCam, IS_PARAMETERSET_CMD_SAVE_EEPROM, NULL, 0 )!= IS_SUCCESS){
        is_ExitCamera(master_hCam);
        cout<<"[Master] : Camera "<<master_hCam<<" CLOSED !!!!!! Error"<<endl;
        is_ExitCamera(slave_hCam);
        cout<<"[Slave] : Camera "<<slave_hCam<<" CLOSED !!!!!! Error"<<endl;
        return false;
    }
    cout<<"[Master] : Parameters Save Done"<<endl;

    if(is_ParameterSet( slave_hCam, IS_PARAMETERSET_CMD_SAVE_EEPROM, NULL, 0 )!= IS_SUCCESS){
        is_ExitCamera(master_hCam);
        cout<<"[Master] : Camera "<<master_hCam<<" CLOSED !!!!!! Error"<<endl;
        is_ExitCamera(slave_hCam);
        cout<<"[Slave] : Camera "<<slave_hCam<<" CLOSED !!!!!! Error"<<endl;
        return false;
    }
    cout<<"[Slave] : Parameters Save Done"<<endl;

    is_ExitCamera(master_hCam);
    cout<<"[Master] : Camera "<<master_hCam<<" CLOSED"<<endl;
    is_ExitCamera(slave_hCam);
    cout<<"[Slave] : Camera "<<slave_hCam<<" CLOSED"<<endl;

    sleep(5);

    return true;
}

void MainWindow::UpdateGUI(){
    if (ui->radioButton_stereo->isChecked() && master != NULL){
        ui->lcdNumber_synch_error->display((double)master->GetTimeError());
        //cout<<"Synchronization Error (ms) : "<<(double)master->GetTimeError()<<endl;
    }
}

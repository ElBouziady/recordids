#ifndef BUFFERS_H
#define BUFFERS_H

#include <QMutex>
#include <QList>
#include <opencv2/opencv.hpp>

typedef enum{
    FrameStatusNoFrame,
    FrameStatusImageInit,
    FrameStatusSaved
}FrameStatus;

/**
 * @brief Une classe qui gere le buffers de traitement (Data flow).
 */
class ImageFrame
{

public:
    /**
     * @brief Le constructeur.
     */
    ImageFrame(int width, int height);

    /**
     * @brief Le destructeur.
     */
    ~ImageFrame();


    /**
     * @brief Recuperer l'image d'entree.
     * @param image_input : une reference vers l'image d'entree.
     * @return recuperation OK.
     */
    bool getImageInput(cv::Mat &image_input);

    /**
     * @brief Recuperer le status.
     * @return Le status.
     */
    FrameStatus getStatus();

    /**
     * @brief Recuperer l'indice du la trame.
     * @return l'indice du la trame.
     */
    int getIndexFrame();



    /**
     * @brief Modifier l'image d'entree.
     * @param image : l'image d'entree.
     * @param index : l'indice de la trame.
     */
    void setImageInput(cv::Mat* image, int index);


    /**
     * @brief Modifier le status vers Saved.
     */
    void setStatusSaved();

    /**
     * @brief Modifier le status vers non uniforme (pour ignorer cette trame).
     */
    void setStatusNoFrame();

    void setStatusImageInit();

private:

    /**
     * @brief L'indice de la trame.
     */
    int index_frame_;
    /**
     * @brief Le buffer des images brutes
     */
    cv::Mat image_input_;

    /**
     * @brief Le buffer des images brutes 2 (non utiliser actuellement).
     */
    //cv::Mat image_input_second_;


    /**
     * @brief l'etat de la trame.
     */
    FrameStatus frame_status_;

    /**
     * @brief Un mutex pour interdir l'utilisation de la meme zone memoir (data_processing) de deux thread different.
     */
    QMutex* frame_mutex_;

};

class Buffers
{
public:
    Buffers(int width, int height);

    ~Buffers();


    /**
     * @brief Initialiser le buffers des images
     */
    void ClearDataProcessing();

    int CountUnsavedImages();

    public:
    /**
     * @brief La taille des buffers circulaire
     */
    int size_buffer_images_;

    /**
     * @brief les donnees de traitement
     */
    QList<ImageFrame > data_processing_;

    /**
     * @brief compteur des images traitees comme input
     */
    int count_image_input_;

    /**
     * @brief compteur des images traitees comme saved
     */
    int count_image_saved_;

    int width_;
    int height_;
    bool recording_active_;
};


#endif // BUFFERS_H

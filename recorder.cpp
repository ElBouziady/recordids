#include "recorder.h"

using namespace std;
using namespace cv;


Recorder::Recorder(Buffers* buffer)
{
    buffer_ = buffer;
}

Recorder::~Recorder(){
    delete buffer_;
    delete video_file_;
}

void Recorder::OpenTimeFile(){
    int index = record_dir_.lastIndexOf("/");
    time_file_.setFileName(QString::fromStdString(record_dir_.toStdString().substr(0,index))+"/time_file.txt");
    //cout<<record_dir_.toStdString()<<"ffffffff"<<endl;
    time_file_.open(QIODevice::WriteOnly | QIODevice::Append);
    time_file_text_.setDevice(&time_file_);
}

void Recorder::CloseTimeFile(){
    time_file_.close();
}

void Recorder::SetVideoFile(VideoWriter* video_file){
    video_file_ = video_file;
}

void Recorder::SetVideosOrImages(bool is_video){
    is_video_ = is_video;
}

void Recorder::SetRole(string camera_role){
    role_ = camera_role;
}

void Recorder::SetRecordDir(QString dir_path){
    record_dir_ = dir_path;
}


void Recorder::run()
{

    while(buffer_->recording_active_ || buffer_->CountUnsavedImages())
    {

        if(buffer_->count_image_saved_ < buffer_->count_image_input_)
        {
            cout<<role_<<" buffer_->count_image_saved_ = "<<buffer_->count_image_saved_<<"!!!!!!!!!!"<<endl<<endl;

            int index_list = buffer_->count_image_saved_ % buffer_->data_processing_.length();

            if(!buffer_->data_processing_[index_list].getImageInput(img_input_))
            {
                cout<<endl<<"Recorder :: Image input perdu...!"<<endl;
                buffer_->count_image_saved_++;
                continue;
            }

            if (time_file_.isOpen())
                time_file_text_<<"Frame "<<buffer_->count_image_saved_<<"     "<<QDate::currentDate().toString("yyyy-MM-dd")<<"-"<<QTime::currentTime().toString("hh-mm-ss-zzz")<<endl;


            if (is_video_)
                video_file_->write(img_input_);
            else {
                ostringstream path;
                path << record_dir_.toStdString().c_str() << "/image" ;
                path << std::setw(5) << std::setfill('0') <<buffer_->count_image_saved_;
                path << ".bmp" ;
                imwrite(path.str(),img_input_);
            }

            buffer_->data_processing_[index_list].setStatusSaved();

            buffer_->count_image_saved_++;

        }
    }
}

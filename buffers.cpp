#include "buffers.h"

using namespace std;
using namespace cv;

Buffers::Buffers(int width, int height)
{
    size_buffer_images_ = 100;
    width_ = width;
    height_ = height;

    for(int i = 0 ; i< size_buffer_images_;i++)
        data_processing_.append(ImageFrame(width_,height_));

    count_image_input_ = 0;
    count_image_saved_ = 0;
    recording_active_ = false;
}

void Buffers::ClearDataProcessing()
{

    count_image_input_ = 0;
    count_image_saved_ = 0;
    recording_active_ = false;

    data_processing_.clear();
    for(int i = 0 ; i< size_buffer_images_;i++)
        data_processing_.append(ImageFrame(width_,height_));


}


int Buffers::CountUnsavedImages(){
    int c = 0;
    for(int i = 0 ; i< size_buffer_images_;i++)
        if (data_processing_[i].getStatus() == FrameStatusImageInit)
            c++;

    return c;
}

Buffers::~Buffers()
{
    data_processing_.clear();
}

ImageFrame::ImageFrame(int width, int height)
{
    image_input_.create(width,height,CV_8UC1);
    //image_input_second_.create(width,height,CV_8UC1);

    frame_mutex_ = new QMutex;
}


ImageFrame::~ImageFrame()
{
    image_input_.release();
    //image_input_second_.release();
}


void ImageFrame::setStatusSaved()
{
    if(frame_status_ == FrameStatusImageInit)
    {
        frame_status_ = FrameStatusSaved;
    }
}

void ImageFrame::setStatusImageInit()
{
    if(frame_status_ == FrameStatusNoFrame)
    {
        frame_status_ = FrameStatusImageInit;
    }
}

void ImageFrame::setStatusNoFrame()
{
    frame_status_ = FrameStatusNoFrame;
}

FrameStatus ImageFrame::getStatus()
{
    return frame_status_;
}

int ImageFrame::getIndexFrame()
{
    return index_frame_;
}

void ImageFrame::setImageInput(Mat* image, int index)
{

    while(true)
    {
        if(frame_mutex_->tryLock())
        {
            setStatusNoFrame();
            image_input_.release();

            index_frame_ = index;

            if(image->empty() && index < 0)
            {
                return ;
            }
            image->copyTo(image_input_);


            setStatusImageInit();
            frame_mutex_->unlock();
            break;
        }
    }
}


bool ImageFrame::getImageInput(Mat &image_input)
{

    if(frame_status_ == FrameStatusNoFrame)
    {
        cout<<endl<<"Image input perdu...!"<<endl;
        return false;
    }

    while(true)
    {
        if(frame_mutex_->tryLock())
        {
            image_input_.copyTo(image_input);
            frame_mutex_->unlock();
            break;
        }
    }

    return true;


}

